#!/bin/bash

setenforce 0
sed -i "s/SELINUX=enforcing/SELINUX=permissive/" /etc/selinux/config
dnf install docker docker-compose vim -y
systemctl start docker
systemctl enable docker

cat > wordpress3-docker.yml << "EOF"
version: '3'
services:
  nginx-proxy:
    image: jwilder/nginx-proxy
    container_name: nginx-proxy
    restart: always
    ports:
      - "80:80"
      - "443:443"
    volumes:
      - /var/run/docker.sock:/tmp/docker.sock:ro
      - /root/certs:/etc/nginx/certs:ro
      - /etc/nginx/vhost.d
      - /usr/share/nginx/html
      - /root/html:/var/www/html
    labels:
      - com.github.jrcs.letsencrypt_nginx_proxy_companion.nginx_proxy

  letsencrypt:
    image: jrcs/letsencrypt-nginx-proxy-companion
    restart: always
    volumes:
      - /root/certs:/etc/nginx/certs:rw
      - /var/run/docker.sock:/var/run/docker.sock:ro
    volumes_from:
      - nginx-proxy

  db:
    container_name: wp-mysql
    image: mysql:5.7
    restart: always
    volumes:
      - /root/db/data:/var/lib/mysql
    environment:
       MYSQL_ROOT_PASSWORD: wordpresspass
       MYSQL_DATABASE: wordpress
       MYSQL_USER: wordpressuser
       MYSQL_PASSWORD: wordpresspass
    ports:
      - "3306:3306"

  wp:
    container_name: wp-web
    volumes:
      - /root/wp/html:/var/www/html
    volumes_from:
      - letsencrypt
    depends_on:
      - db
    image: wordpress
    restart: always
    expose:
      - "80"
      - "443"

    environment:
     VIRTUAL_HOST: misnotas.dextre.xyz
     LETSENCRYPT_HOST: misnotas.dextre.xyz
     LETSENCRYPT_EMAIL: jhontitor@gmail.com
     WORDPRESS_DB_HOST: db:3306
     WORDPRESS_DB_USER: wordpressuser
     WORDPRESS_DB_PASSWORD: wordpresspass

EOF

docker-compose -f wordpress3-docker.yml up -d

docker ps
